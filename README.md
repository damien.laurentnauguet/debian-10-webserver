# Pre-requisites

Before starting, you need:
- A domain name (i.e.: namecheap)
- A server running on Debian 19 (i.e.: scaleway)
- Optionnal: a DNS manager (i.e.: cloudflare)

# Packages

Once connected to server, please install following packages:
- fail2ban
- ufw
- sudo
- curl
- git
- zsh
- sysvinit-utils
- nginx

`apt update && apt install fail2ban ufw sudo curl git zsh sysvinit-utils nginx`

> Add user to sudoers: `usermod -aG sudo <username>`

# Configure firewall

Create a script as root at `~/ufw/start.sh`, and copy following script:

```sh
#!/bin/bash

# Script must be run as root, so check EUID in user's env
if [ "$EUID" -ne 0 ]
	then echo "Please run as root"
	exit 1
fi

# Reset ufw
echo "Reset ufw service"
ufw --force reset

# Start ufw with default rule
echo "Start ufw with default rule"
ufw --force enable

# Allow loggin
echo "Enable medium logging"
ufw logging medium

# Allow exposed ports
ufw allow 9000 # SSH port you will set after
ufw allow 80 # HTTP
ufw allow 443 # SSL

# Fetch latest versions of cloudflare’s ipv4/6
wget https://www.cloudflare.com/ips-v4 -O ips-v4.tmp
wget https://www.cloudflare.com/ips-v6 -O ips-v6.tmp
mv ips-v4.tmp ips-v4
mv ips-v6.tmp ips-v6

for cfip in `cat ips-v4`; do ufw allow from $cfip to any port 80,443 proto tcp comment “Cloudflare IPV4”; done
for cfip in `cat ips-v6`; do ufw allow from $cfip to any port 80,443 proto tcp comment “Cloudflare IPV6"; done
```

Run above script and ensure ufw is active by running `ufw status`.

Activate ufw at boot: `systemctl enable ufw`


# Configure SSH

Edit `sudo etc/ssh/sshd_config` file and perform following modifications:
- change PORT value (i.e.: 9000)
- Set `PermitRootLogin no`
- Set `AllowUsers <username>`
- Set `PubkeyAuthentication yes`
- Set `PermitRootLogin no`
- Set `PasswordAuthentication no`

On your own machine, generate a ssh key using following command:  
`ssh-keygen -t rsa -b 4096 -C "<email_address>"`

> Tip: save generated files into `~/.ssh/some_memorable_name`

Generated key.pub file content should be saved on VM in:  
`mkdir  ~/.ssh && touch ~/.ssh/authorized_keys && chmod 600 ~/.ssh/authorized_keys` [~ of user, not of root]

Restart your ssh service  
`systemctl restart ssh`

Give it a try, ssh your server from your machine:  
`ssh <username>@<ip_address> -p <port> -i <path_to_your_key>`

# Configure Nginx

Edit `/etc/nginx/nginx.conf` and uncomment `server_tokens` to hide Nginx version.

# Install NodeJS

First, ensure NodeJS version compatibility with debian version:  
`sudo curl -sL https://deb.nodesource.com/setup_12.x -o nodesource_setup.sh`

Then, install NodeJS:  
`sudo apt install nodejs`

Verify installation: `node -v`

# Install MongoDB

Download mongo source and add apt-key:  
`curl https://www.mongodb.org/static/pgp/server-4.2.asc | sudo apt-key add -`

Edit `nano /etc/apt/sources.list.d/mongodb-org-4.2.list` and paste:  
`deb http://repo.mongodb.org/apt/debian buster/mongodb-org/4.2 main`

# Configure LET'S ENCRYPT with Nginx

First, install required packages:  
`sudo apt install python3-acme python3-certbot python3-mock python3-openssl python3-pkg-resources python3-pyparsing python3-zope.interface`  
`sudo apt install python3-certbot-nginx`  

Browse to `/etc/nginx/sites-available/`.  
A single file named `default` should be present. Delete it. Delete also its link in `/etc/nginx/sites-enabled/default`.

Now, create a new file named by the name of your domain and paste the following:
```
server {
	listen 443 ssl;
	server_name <domain_name> www.<domain_name>;
    ssl_certificate /etc/letsencrypt/live/<domain_name>/fullchain.pem;
	ssl_certificate_key /etc/letsencrypt/live/<domain_name>/privkey.pem;
	location / {
		proxy_pass http://127.0.0.1:4000;
	}
}
```

Now, create another file named `force-ssl` with following content:
```
server {
	listen 80 default_server;
	server_name _ <domain_name> www.<domain_name>;
	return https://$host$request_uri;
}
```

Create symbolic links:  
`ln -s /etc/nginx/sites-available/<domain_name> /etc/nginx/sites-enabled`  
`ln -s /etc/nginx/sites-available/force-ssl /etc/nginx/sites-enabled` 

Generate certificate (wildcard cert) and add DNS record asked by instructions:
`certbot certonly --manual -d "*.<domain_name>"`

Generated certificates are stored in `/etc/letsencrypt/live/<domain_name>`

Restart Nginx: `service nginx restart`

# Configure your DNS 

Create 2 DNS records:
- A:<domain_name>:<server_ip> 
- A:www:<server_ip> 

(You should have 3 DNS records in your configuration)
